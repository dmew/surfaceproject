using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using System.Drawing;
using System.Drawing.Imaging;


namespace SurfaceApplication2
{
    /// <summary>
    /// Interaction logic for SurfaceWindow1.xaml
    /// </summary>
    public partial class SurfaceWindow1 : SurfaceWindow
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SurfaceWindow1()
        {
            InitializeComponent();

            // Add handlers for Application activation events
            AddActivationHandlers();
            System.Drawing.Image img = System.Drawing.Image.FromFile("C:\\Users\\Dani\\Downloads\\Documents\\Visual Studio 2008\\Projects\\SurfaceApplication2\\SurfaceApplication2\\Resources\\smile.jpg");

            Bitmap bmp = new Bitmap(img.Width, img.Height);
            Graphics graphics = Graphics.FromImage(bmp);
            graphics.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
            
        }


        /// <summary>
        /// Occurs when the window is about to close. 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            // Remove handlers for Application activation events
            RemoveActivationHandlers();
        }

        /// <summary>
        /// Adds handlers for Application activation events.
        /// </summary>
        private void AddActivationHandlers()
        {
            // Subscribe to surface application activation events
            ApplicationLauncher.ApplicationActivated += OnApplicationActivated;
            ApplicationLauncher.ApplicationPreviewed += OnApplicationPreviewed;
            ApplicationLauncher.ApplicationDeactivated += OnApplicationDeactivated;
        }

        /// <summary>
        /// Removes handlers for Application activation events.
        /// </summary>
        private void RemoveActivationHandlers()
        {
            // Unsubscribe from surface application activation events
            ApplicationLauncher.ApplicationActivated -= OnApplicationActivated;
            ApplicationLauncher.ApplicationPreviewed -= OnApplicationPreviewed;
            ApplicationLauncher.ApplicationDeactivated -= OnApplicationDeactivated;
        }

        /// <summary>
        /// This is called when application has been activated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnApplicationActivated(object sender, EventArgs e)
        {
            //TODO: enable audio, animations here
        }

        /// <summary>
        /// This is called when application is in preview mode.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnApplicationPreviewed(object sender, EventArgs e)
        {
            //TODO: Disable audio here if it is enabled

            //TODO: optionally enable animations here
        }

        /// <summary>
        ///  This is called when application has been deactivated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnApplicationDeactivated(object sender, EventArgs e)
        {
            //TODO: disable audio, animations here
        }

        private void startbutton_Click(object sender, RoutedEventArgs e)
        {
            startbutton.Visibility = Visibility.Hidden;
            //System.Drawing.Image image1= System.Drawing.Image.FromFile("Resources/Icon.png");
           // Bitmap bm1 = new Bitmap(image1);

            //Graphics g = Graphics.FromImage(bm1);
           // g.DrawImage(image1, 0, 0, image1.Width, image1.Height);
       
            
           
        }

        private void transparancy_Click(object sender, RoutedEventArgs e)
        {

        }

        private void checkerboardbutton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void myslider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            float transfloat = (float)myslider.Value/100;
            System.Drawing.Image img = System.Drawing.Image.FromFile("C:\\Users\\Dani\\Downloads\\Documents\\Visual Studio 2008\\Projects\\SurfaceApplication2\\SurfaceApplication2\\Resources\\smile.jpg");
            setImageOpacity(img, transfloat);
            
            label1.Content = ((myslider.Value /myslider.Maximum) * 100) + "%";
            
            


        }

        private void setImageOpacity(System.Drawing.Image img, float transfloat )
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height);
            Graphics graphics = Graphics.FromImage(bmp);
            ColorMatrix colormatrix = new ColorMatrix();
            colormatrix.Matrix33 = transfloat;
            ImageAttributes imgAttribute = new ImageAttributes();
            imgAttribute.SetColorMatrix(colormatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

            //this line should draw image with transparancy set
            //graphics.DrawImage(img, new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imgAttribute);
            //test line to see if working correctly... its not!
            graphics.DrawImage(img, 0, 0,bmp.Width,bmp.Height);
            
            graphics.Dispose();   // Releasing all resource used by graphics 
           

        }

        private void checkerbutton_Click(object sender, RoutedEventArgs e)
        {

        }

    }
}